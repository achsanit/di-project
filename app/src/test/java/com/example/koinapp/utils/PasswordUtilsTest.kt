package com.example.koinapp.utils

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

internal class PasswordUtilsTest {

    lateinit var passwordUtils: PasswordUtils
    lateinit var pass: String

    @Before
    fun setUp() {
        passwordUtils= PasswordUtils()
        pass = "Apple123"
    }

    @Test
    fun `validate 1 uppercase return true`(){
        assertTrue(pass.matches(".*[A-Z].*".toRegex()))
    }

    @Test
    fun `validate 1 lowercase return true`() {
        assertTrue(pass.matches(".*[a-z].*".toRegex()))
    }

    @Test
    fun  `validate 1 number return true`() {
        assertTrue(pass.matches(".*[0-9].*".toRegex()))
    }

    @Test
    fun `validate more 7 character return false`() {
        assertFalse(pass.length < 8)
    }

    //test validate() function
    @Test
    fun validate() {
        assertTrue(passwordUtils.validate("Apple123"))
    }

    @Test
    fun `validate return false`() {
        assertFalse(passwordUtils.validate("achsanit"))
    }
}