package com.example.koinapp.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.koinapp.R
import com.example.koinapp.data.model.User
import kotlinx.android.synthetic.main.item_layout.view.*

class MainAdapter(private val users: ArrayList<User>) : RecyclerView.Adapter<MainAdapter.DataViwHolder>() {
    class DataViwHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViwHolder {
        return DataViwHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false))
    }

    override fun onBindViewHolder(holder: DataViwHolder, position: Int) {
        val currentUser = users[position]
        holder.itemView.tvUsername.text = currentUser.name
        holder.itemView.tvEmail.text = currentUser.email
        Glide.with(holder.itemView.ivAvatar.context)
            .load(currentUser.avatar)
            .into(holder.itemView.ivAvatar)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    fun addData(list: List<User>) {
        users.addAll(list)
    }
}