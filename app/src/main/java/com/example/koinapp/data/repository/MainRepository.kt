package com.example.koinapp.data.repository

import com.example.koinapp.data.api.ApiHelper

class MainRepository (private val apiHelper: ApiHelper) {
    suspend fun getUsers() = apiHelper.getUsers()
}