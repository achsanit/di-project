package com.example.koinapp.data.api

import com.example.koinapp.data.model.User
import retrofit2.Response

interface ApiHelper {
    suspend fun getUsers(): Response<List<User>>
}