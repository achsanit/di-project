package com.example.koinapp.di.module

import com.example.koinapp.data.repository.MainRepository
import org.koin.dsl.module

val repoModule = module {
    single { MainRepository(get()) }
}