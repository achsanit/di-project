package com.example.koinapp

import android.app.Application
import com.example.koinapp.di.module.appModule
import com.example.koinapp.di.module.repoModule
import com.example.koinapp.di.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@App)
            modules(appModule, repoModule, viewModelModule)
        }
    }
}