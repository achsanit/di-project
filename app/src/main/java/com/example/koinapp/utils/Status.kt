package com.example.koinapp.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}